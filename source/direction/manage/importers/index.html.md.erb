---
layout: markdown_page
title: "Category Direction - Importers"
description: "This GitLab group is focused on enabling GitLab.com adoption through various importers, mainly GitLab Migration and GitHub importer. Find more information here!"
canonical_path: "/direction/manage/importers/"
---

- TOC
{:toc}

## Importers

| Section | Maturity | Last Reviewed |
| --- | --- | --- |
| [Dev](/direction/dev/) | Non-marketable | 2022-12-05 |


## Introduction and how you can help

The Importers direction page belongs to [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group within the [Manage Stage](https://about.gitlab.com/handbook/product/categories/#manage-stage) of the [Dev](/handbook/product/categories/#dev-section)
section, and is maintained by [Magdalena Frankiewicz](https://gitlab.com/m_frankiewicz).

This vision is a work in progress and everyone can contribute. 
If you'd like to provide feedback or contribute to this vision, please feel free to comment directly on issues and epics at GitLab.com.

## Mission

<%= partial("direction/manage/importers/templates/overview") %>

## Where we are headed

Supporting the [GitLab-hosted First](https://about.gitlab.com/direction/#gitlab-hosted-first) product theme, the Import group is working towards making imports reliable and performant at any scale. Large-scale moves to GitLab should be significantly easier and ultimately reach the first-class level exeprience. 

Areas of interest and improvement can be organized by the following goals:

* **Ease of use**
Customers looking to import their data sometimes struggle to find the place in GitLab where they can initiate imports. Once found, the user interactions are not always intuitive and the flow is not fully user-friendly. Improving the user experience in this area will make our importers more lovable.

* **Reliability**
A portion of our current issues are related to the reliability of the solution. Imports don't always succeed and when they fail, there is little guidance for the user on steps they can take to remedy the failure. We are working on making our importers more reliable, so that our customers can have confidence in the migration process.

* **Scalability and performance**
Large organizations looking to move possibly hundreds or thousands of projects from GitHub to GitLab or from self-managed GitLab instance to GitLab.com need to be able to do so in a performant way. 

## What's next & why

<%= partial("direction/manage/importers/templates/next") %>

## What is not planned right now

<%= partial("direction/manage/importers/templates/not_doing") %>

## Recent accomplishments
#### Migrating groups and project by direct transfer between GitLab instances
* We now [preserve association of migrated MRs to issue](https://gitlab.com/gitlab-org/gitlab/-/issues/267947), which populates the "Related merge requests" section on the issue details page.

#### Importing projects from GitHub
* Users can now [opt-in to import more relationships when importing projects from GitHub](https://gitlab.com/gitlab-org/gitlab/-/issues/370734). Importing all supported relationships can take a long time and is disabled by default. However, users can now [choose to import](https://docs.gitlab.com/ee/user/project/import/github.html#select-additional-items-to-import):
  - [Image resources and other attachments types](https://gitlab.com/gitlab-org/gitlab/-/issues/18052)
  - [Issue events](https://gitlab.com/groups/gitlab-org/-/epics/7655)
  - [Pull request events](https://gitlab.com/groups/gitlab-org/-/epics/7673)
  - [All pull request and issue comments](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/67150) (delivered in GitLab 14.2.).
* Reviewers assigned to pull requests in GitHub are now [imported as reviewers assigned
to merge requests in GitLab](https://gitlab.com/gitlab-org/gitlab/-/issues/355137).
* Most of GitHub branch protection rules that have an equivalent on GitLab are mapped to GitLab branch protection rules or project-wide GitLab settings (see in [documentation](https://docs.gitlab.com/ee/user/project/import/github.html#branch-protection-rules-and-project-settings)).

## Overview of existing importers

This table provides a quick overview of what GitLab importers exist today and which most important objects they each support. This list is not exhaustive and the detailed information can be found on the [Importers documentation page](https://docs.gitlab.com/ee/user/project/import/).

[tanuki]: https://about.gitlab.com/ico/favicon-16x16.png "GitLab"
[tan2]: <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.25em" aria-hidden="true">

| Import source                                                                                 | Repos       | MRs        | Issues     | Epics     | Milestones | Wiki       | Designs   | API <sup>*</sup> |
|-----------------------------------------------------------------------------------------------|-------------|------------|------------|-----------|------------|------------|-----------|------------|
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> GitLab Migration](https://docs.gitlab.com/ee/user/group/import/)           | ✅          | ✅          | ✅          | ✅       | ✅          | ✅         | ✅        | ✅         |
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Group Import/Export](https://docs.gitlab.com/ee/user/group/settings/import_export.html)     | ➖ | ➖         | ➖          | ✅       | ✅          | ➖         | ➖        | ✅         |
| [<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:1.0em" aria-hidden="true"></i> Project Import/Export](https://docs.gitlab.com/ee/user/project/settings/import_export.html) | ✅ | ✅         | ✅          | ➖       | ✅          | ✅         | ✅        | ✅         |
| [GitHub](https://docs.gitlab.com/ee/user/project/import/github.html)                          | ✅          | ✅          | ✅          | ➖       | ✅          | ✅         | ➖        | ✅         |
| [Bitbucket Cloud](https://docs.gitlab.com/ee/user/project/import/bitbucket.html)              | ✅          | ✅          | ✅          | ➖       | ✅          | ✅         | ➖        | ❌         |
| [Bitbucket Server](https://docs.gitlab.com/ee/user/project/import/bitbucket_server.html)      | ✅          | ✅          | ❌          | ➖       | ❌          | ➖         | ➖        | ✅         |
| [Gitea](https://docs.gitlab.com/ee/user/project/import/gitea.html)                            | ✅          | ✅          | ✅          | ➖       | ✅          | ➖         | ➖        | ❌         |
| [Git (Repo by URL)](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html)          | ✅          | ✅          | ➖          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [Manifest file](https://docs.gitlab.com/ee/user/project/import/manifest.html)                 | ✅          | ✅          | ➖          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [CSV](https://docs.gitlab.com/ee/user/project/issues/csv_import.html)                         | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [FogBugz](https://docs.gitlab.com/ee/user/project/import/fogbugz.html)                        | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |
| [Phabricator](https://docs.gitlab.com/ee/user/project/import/phabricator.html)                | ➖          | ➖          | ✅          | ➖       | ➖          | ➖         | ➖        | ❌         |

* ✅ : Supported
* ❌ : Not supported
* ➖ : Not applicable

**_<sup>*</sup> This column indicates whether this importer is accessible via API, in addition to the UI._**

## Maturity Plan

Importers is a **non-marketable category**, and is therefore not assigned a maturity level. However, we use [GitLab's Maturity framework](https://about.gitlab.com/direction/maturity/) to visualize the current state of the high priority importers and discuss their future roadmap. Generally speaking:

* A Minimal importer is only usable via the API, with only basic importing capabilities and no UI controls.
* A Viable importer has a UI component, but imports an incomplete set of objects. Users are not included.
* A Complete importer is stable, handles large-scale imports, and imports over most objects.
* A Lovable importer has a terrific user experience, recovers gracefully from errors, and imports over nearly 100% of relevant objects.

#### GitLab single group and single project import with export file
The historical, but still in use GitLab group and project importer functionality consists of two separate importers, the [(single) group import/export](https://docs.gitlab.com/ee/user/group/settings/import_export.html) and [(single) project import/export](https://docs.gitlab.com/ee/user/project/settings/import_export.html). Together, they allow for group and project migrations between two instances of GitLab using data files, one group/project at a time.

Migrating an entire group with projects therefore requires coordination of multiple exports and imports, including some manual data manipulation and user configuration. This user experience creates a lot of friction.

Both the single group and the single project importers are available through the API and the UI and most of the objects in each are being exported and imported, which makes them a **Viable** feature in GitLab. 

We have recognized that larger customers have needs that exceed the capabilities of those file-based importers. Therefore, we are focused on developing a fast and scalable replacement where group with all their subgroups and projects can be migrated by direct transfer.

#### Migrating groups and projects by direct

With this new way of migrating groups and project we aim for a first-class GitLab-to-GitLab migration experience.

Once migrating groups and projects by direct transfer is ready for production use at any scale (currently we are preparing for [releasing to open beta](https://gitlab.com/groups/gitlab-org/-/epics/9285)), the single group and single project import/export will be [disabled by feature flag](https://docs.gitlab.com/ee/administration/feature_flags.html), and only migrating groups and projects by direct transfer will be available in the UI and API. 
At first, customers that are using air-gapped networks, with no network connectivity between their GitLab instances, will need to [enable](https://docs.gitlab.com/ee/administration/feature_flags.html) the export/import solution. They won't be able to use the new way of migrating groups and projects, as it requires a direct connection between the migrated instances, until we extend this solution to [support also the air-gapped solutions](https://gitlab.com/groups/gitlab-org/-/epics/8985).

#### GitHub Importer

The [GitHub Importer](https://docs.gitlab.com/ee/user/project/import/github.html) is currently a **Viable** feature in GitLab. It is accessible through both the API and the UI and uses a direct link to the GitHub server to import the highest priority objects, such as merge requests, issues, milestones and wiki pages, in addition to the repository.

#### Bitbucket Cloud Importer

The [Bitbucket Cloud Importer](https://docs.gitlab.com/ee/user/project/import/bitbucket.html) is currently a **Viable** feature in GitLab. It is accessible through the UI and uses a direct link to the Bitbucket Cloud repository to import the highest priority objects, such as merge requests, issues, and milestones, in addition to the repository. 

There is currently no ongoing work to achieve the **Complete** maturity level for Bitbucket Cloud Importer. This maturity level would include most of the available objects and ability to invoke the import via the API.

#### Git Importer

The [Git Importer](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) is currently a **Complete** feature in GitLab. This is a basic importer that migrates any Git repository into GitLab. Most of our competitors (i.e. GitHub, Microsoft Azure, Atlassian Bitbucket) only offer this type of importer.

There is currently no ongoing work to achieve the **Lovable** maturity level for Git Importer. This maturity level would include an improved user experience.

## Automating group and project import with Professional Services

While the long-term goal for the Import group is to provide all the GitLab importing capabilities needed by our customers in our application, we recognize that GitLab's current capabilities may not support specific migration scenarios. Often, we're not aware of these requirements until a large customer provides us with specific migration requirements.

GitLab [Professional Services](https://about.gitlab.com/services/) team uses [Congregate](https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/congregate) tool to orchestrate user, group, and project import API calls in order to help customers automate scaled migrations. With the feature of migrating groups and projects by direct transfer ready for production use at any scale, we will be able to substitute the part of Congregate handling migration on groups and projects.

#### Note

* While the Import group’s main focus is Importers, other groups may choose to contribute to individual Importers based on their strategic importance and adoption of their features. This is in keeping with GitLab’s mission that [everyone can contribute](https://about.gitlab.com/handbook/values/#mission). 

If you'd like to contribute feedback on areas you'd like to see prioritized, please add them as comments in the corresponding [epic](https://gitlab.com/groups/gitlab-org/-/epics/2721) for this category.
